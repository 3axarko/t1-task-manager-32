package ru.t1.zkovalenko.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.endpoint.ISystemEndpoint;
import ru.t1.zkovalenko.tm.api.endpoint.IDomainEndpoint;
import ru.t1.zkovalenko.tm.api.endpoint.IProjectEndpoint;
import ru.t1.zkovalenko.tm.api.endpoint.ITaskEndpoint;
import ru.t1.zkovalenko.tm.api.endpoint.IUserEndpoint;
import ru.t1.zkovalenko.tm.api.repository.IProjectRepository;
import ru.t1.zkovalenko.tm.api.repository.ITaskRepository;
import ru.t1.zkovalenko.tm.api.repository.IUserRepository;
import ru.t1.zkovalenko.tm.api.service.*;
import ru.t1.zkovalenko.tm.dto.request.data.*;
import ru.t1.zkovalenko.tm.dto.request.project.*;
import ru.t1.zkovalenko.tm.dto.request.system.ServerAboutRequest;
import ru.t1.zkovalenko.tm.dto.request.system.ServerVersionRequest;
import ru.t1.zkovalenko.tm.dto.request.task.*;
import ru.t1.zkovalenko.tm.dto.request.user.*;
import ru.t1.zkovalenko.tm.endpoint.*;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.model.Project;
import ru.t1.zkovalenko.tm.model.User;
import ru.t1.zkovalenko.tm.repository.ProjectRepository;
import ru.t1.zkovalenko.tm.repository.TaskRepository;
import ru.t1.zkovalenko.tm.repository.UserRepository;
import ru.t1.zkovalenko.tm.service.*;
import ru.t1.zkovalenko.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    @NotNull
    private final IDomainService domainService = new DomainService(this);

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::dataBackupLoad);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::dataBackupSave);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::dataBase64Load);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::dataBase64Save);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::dataBinaryLoad);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::dataBinarySave);
        server.registry(DataJsonLoadJaxBRequest.class, domainEndpoint::dataJsonLoadJaxB);
        server.registry(DataJsonLoadJFasterXmlRequest.class, domainEndpoint::dataJsonLoadJFasterXml);
        server.registry(DataJsonSaveFasterXmlRequest.class, domainEndpoint::dataJsonSaveFasterXml);
        server.registry(DataJsonSaveJaxBRequest.class, domainEndpoint::dataJsonSaveJaxB);
        server.registry(DataXmlLoadJaxBRequest.class, domainEndpoint::dataXmlLoadJaxB);
        server.registry(DataXmlLoadJFasterXmlRequest.class, domainEndpoint::dataXmlLoadJFasterXml);
        server.registry(DataXmlSaveFasterXmlRequest.class, domainEndpoint::dataXmlSaveFasterXml);
        server.registry(DataXmlSaveJaxBRequest.class, domainEndpoint::dataXmlSaveJaxB);
        server.registry(DataYamlLoadFasterXmlRequest.class, domainEndpoint::dataYamlLoadFasterXml);
        server.registry(DataYamlSaveFasterXmlRequest.class, domainEndpoint::dataYamlSaveFasterXml);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::projectChangeStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::projectChangeStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::projectClear);
        server.registry(ProjectCompleteByIdRequest.class, projectEndpoint::projectCompleteById);
        server.registry(ProjectCompleteByIndexRequest.class, projectEndpoint::projectCompleteByIndex);
        server.registry(ProjectCreateRequest.class, projectEndpoint::projectCreate);
        server.registry(ProjectListRequest.class, projectEndpoint::projectList);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::projectRemoveById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::projectRemoveByIndex);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::projectShowById);
        server.registry(ProjectShowByIndexRequest.class, projectEndpoint::projectShowByIndex);
        server.registry(ProjectStartByIdRequest.class, projectEndpoint::projectStartById);
        server.registry(ProjectStartByIndexRequest.class, projectEndpoint::projectStartByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::projectUpdateById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::projectUpdateByIndex);

        server.registry(TaskBindToProjectRequest.class, taskEndpoint::taskBindToProject);
        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::taskChangeStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::taskChangeStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::taskClear);
        server.registry(TaskCompleteByIdRequest.class, taskEndpoint::taskCompleteById);
        server.registry(TaskCompleteByIndexRequest.class, taskEndpoint::taskCompleteByIndex);
        server.registry(TaskCreateRequest.class, taskEndpoint::taskCreate);
        server.registry(TaskListRequest.class, taskEndpoint::taskList);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::taskRemoveById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::taskRemoveByIndex);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::taskShowById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::taskShowByIndex);
        server.registry(TaskShowByProjectIdRequest.class, taskEndpoint::taskShowByProjectId);
        server.registry(TaskStartByIdRequest.class, taskEndpoint::taskStartById);
        server.registry(TaskStartByIndexRequest.class, taskEndpoint::taskStartByIndex);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::taskUnbindFromProject);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::taskUpdateById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::taskUpdateByIndex);

        server.registry(UserChangePasswordRequest.class, userEndpoint::userChangePassword);
        server.registry(UserLockRequest.class, userEndpoint::userLock);
        server.registry(UserRegistryRequest.class, userEndpoint::userRegistry);
        server.registry(UserRemoveRequest.class, userEndpoint::userRemove);
        server.registry(UserUnlockRequest.class, userEndpoint::userUnlock);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::userUpdateProfile);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        final User firstUser = userService.create("1", "1", "12@mail.ru");
        userService.create("2", "2", "13@mail.ru");
        final User adminUser = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(adminUser.getId(), new Project("project1", Status.IN_PROGRESS));
        projectService.add(adminUser.getId(), new Project("project3", Status.NOT_STARTED));
        projectService.add(adminUser.getId(), new Project("project2", Status.IN_PROGRESS));
        projectService.add(adminUser.getId(), new Project("project4", Status.COMPLETED));

        taskService.create(adminUser.getId(), "task1");
        taskService.create(adminUser.getId(), "task2");
    }

    public void run() {
        initPID();
        initDemoData();
        loggerService.info("** Task-Manager server started **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        server.start();
    }

    private void prepareShutdown() {
        loggerService.info("** Task-Manager server stopped **");
        backup.stop();
        server.stop();
    }

}
