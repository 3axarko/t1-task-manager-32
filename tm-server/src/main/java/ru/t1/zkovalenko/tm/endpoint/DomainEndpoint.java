package ru.t1.zkovalenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.endpoint.IDomainEndpoint;
import ru.t1.zkovalenko.tm.api.service.IServiceLocator;
import ru.t1.zkovalenko.tm.dto.request.data.*;
import ru.t1.zkovalenko.tm.dto.response.data.*;
import ru.t1.zkovalenko.tm.enumerated.Role;

public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public DataBackupLoadResponse dataBackupLoad(@NotNull DataBackupLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBackupLoad();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    public DataBackupSaveResponse dataBackupSave(@NotNull DataBackupSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBackupSave();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    public DataBase64LoadResponse dataBase64Load(@NotNull final DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBase64Load();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    public DataBase64SaveResponse dataBase64Save(@NotNull final DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBase64Save();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse dataBinaryLoad(@NotNull final DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBinaryLoad();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    public DataBinarySaveResponse dataBinarySave(@NotNull final DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataBinarySave();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    public DataJsonLoadJaxBResponse dataJsonLoadJaxB(@NotNull final DataJsonLoadJaxBRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataJsonLoadJaxB();
        return new DataJsonLoadJaxBResponse();
    }

    @NotNull
    @Override
    public DataJsonLoadJFasterXmlResponse dataJsonLoadJFasterXml(@NotNull final DataJsonLoadJFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataJsonLoadJFasterXml();
        return new DataJsonLoadJFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataJsonSaveFasterXmlResponse dataJsonSaveFasterXml(@NotNull final DataJsonSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataJsonSaveFasterXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataJsonSaveJaxBResponse dataJsonSaveJaxB(@NotNull final DataJsonSaveJaxBRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataJsonSaveJaxB();
        return new DataJsonSaveJaxBResponse();
    }

    @NotNull
    @Override
    public DataXmlLoadJaxBResponse dataXmlLoadJaxB(@NotNull final DataXmlLoadJaxBRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataXmlLoadJaxB();
        return new DataXmlLoadJaxBResponse();
    }

    @NotNull
    @Override
    public DataXmlLoadJFasterXmlResponse dataXmlLoadJFasterXml(@NotNull final DataXmlLoadJFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataXmlLoadJFasterXml();
        return new DataXmlLoadJFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataXmlSaveFasterXmlResponse dataXmlSaveFasterXml(@NotNull final DataXmlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataXmlSaveFasterXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataXmlSaveJaxBResponse dataXmlSaveJaxB(@NotNull final DataXmlSaveJaxBRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataXmlSaveJaxB();
        return new DataXmlSaveJaxBResponse();
    }

    @NotNull
    @Override
    public DataYamlSaveFasterXmlResponse dataYamlSaveFasterXml(@NotNull final DataYamlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataYamlSaveFasterXml();
        return new DataYamlSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    public DataYamlLoadFasterXmlResponse dataYamlLoadFasterXml(@NotNull final DataYamlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().dataYamlLoadFasterXml();
        return new DataYamlLoadFasterXmlResponse();
    }

}
