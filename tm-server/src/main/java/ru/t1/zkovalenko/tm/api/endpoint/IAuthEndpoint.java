package ru.t1.zkovalenko.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.request.user.UserLoginRequest;
import ru.t1.zkovalenko.tm.dto.request.user.UserLogoutRequest;
import ru.t1.zkovalenko.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.zkovalenko.tm.dto.response.user.UserLoginResponse;
import ru.t1.zkovalenko.tm.dto.response.user.UserLogoutResponse;
import ru.t1.zkovalenko.tm.dto.response.user.UserViewProfileResponse;

public interface IAuthEndpoint {

    @NotNull
    @SneakyThrows
    UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull
    @SneakyThrows
    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull
    @SneakyThrows
    UserViewProfileResponse profile(@NotNull UserViewProfileRequest request);

}
