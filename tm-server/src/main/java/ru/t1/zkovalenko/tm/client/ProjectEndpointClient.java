package ru.t1.zkovalenko.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.endpoint.IProjectEndpoint;
import ru.t1.zkovalenko.tm.dto.request.project.*;
import ru.t1.zkovalenko.tm.dto.request.user.UserLoginRequest;
import ru.t1.zkovalenko.tm.dto.request.user.UserLogoutRequest;
import ru.t1.zkovalenko.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.zkovalenko.tm.dto.response.project.*;

public class ProjectEndpointClient extends AbstractEndpointClient implements IProjectEndpoint {

    public ProjectEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("1", "1")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserViewProfileRequest()).getUser().getEmail());

        @NotNull final ProjectEndpointClient projectClient = new ProjectEndpointClient(authEndpointClient);
        System.out.println(projectClient.projectCreate(new ProjectCreateRequest("HELLO", "WORLD")));
        System.out.println(projectClient.projectList(new ProjectListRequest()).getProjects());

        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }


    @NotNull
    @Override
    @SneakyThrows
    public ProjectChangeStatusByIdResponse projectChangeStatusById(@NotNull ProjectChangeStatusByIdRequest request) {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectChangeStatusByIndexResponse projectChangeStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request) {
        return call(request, ProjectChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectClearResponse projectClear(@NotNull ProjectClearRequest request) {
        return call(request, ProjectClearResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCompleteByIdResponse projectCompleteById(@NotNull ProjectCompleteByIdRequest request) {
        return call(request, ProjectCompleteByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCompleteByIndexResponse projectCompleteByIndex(@NotNull ProjectCompleteByIndexRequest request) {
        return call(request, ProjectCompleteByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCreateResponse projectCreate(@NotNull ProjectCreateRequest request) {
        return call(request, ProjectCreateResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectListResponse projectList(@NotNull ProjectListRequest request) {
        return call(request, ProjectListResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIdResponse projectRemoveById(@NotNull ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIndexResponse projectRemoveByIndex(@NotNull ProjectRemoveByIndexRequest request) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectShowByIdResponse projectShowById(@NotNull ProjectShowByIdRequest request) {
        return call(request, ProjectShowByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectShowByIndexResponse projectShowByIndex(@NotNull ProjectShowByIndexRequest request) {
        return call(request, ProjectShowByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectStartByIdResponse projectStartById(@NotNull ProjectStartByIdRequest request) {
        return call(request, ProjectStartByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectStartByIndexResponse projectStartByIndex(@NotNull ProjectStartByIndexRequest request) {
        return call(request, ProjectStartByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectUpdateByIdResponse projectUpdateById(@NotNull ProjectUpdateByIdRequest request) {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectUpdateByIndexResponse projectUpdateByIndex(@NotNull ProjectUpdateByIndexRequest request) {
        return call(request, ProjectUpdateByIndexResponse.class);
    }

}
