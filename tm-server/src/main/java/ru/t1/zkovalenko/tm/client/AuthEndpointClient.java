package ru.t1.zkovalenko.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.endpoint.IAuthEndpoint;
import ru.t1.zkovalenko.tm.dto.request.user.UserLoginRequest;
import ru.t1.zkovalenko.tm.dto.request.user.UserLogoutRequest;
import ru.t1.zkovalenko.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.zkovalenko.tm.dto.response.user.UserLoginResponse;
import ru.t1.zkovalenko.tm.dto.response.user.UserLogoutResponse;
import ru.t1.zkovalenko.tm.dto.response.user.UserViewProfileResponse;

@NoArgsConstructor
public class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpoint {

    public AuthEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.profile(new UserViewProfileRequest()).getUser());

        System.out.println(authEndpointClient.login(new UserLoginRequest("admin", "admin")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserViewProfileRequest()).getUser());

        System.out.println(authEndpointClient.login(new UserLoginRequest("1", "1")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserViewProfileRequest()).getUser().getEmail());
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));

        System.out.println(authEndpointClient.profile(new UserViewProfileRequest()).getUser());
        authEndpointClient.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserViewProfileResponse profile(@NotNull final UserViewProfileRequest request) {
        return call(request, UserViewProfileResponse.class);
    }

}
