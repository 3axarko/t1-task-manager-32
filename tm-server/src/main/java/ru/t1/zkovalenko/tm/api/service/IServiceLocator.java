package ru.t1.zkovalenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.client.DomainEndpointClient;

public interface IServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

}
