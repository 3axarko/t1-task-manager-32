package ru.t1.zkovalenko.tm.api.endpoint;

import ru.t1.zkovalenko.tm.dto.request.AbstractRequest;
import ru.t1.zkovalenko.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
