package ru.t1.zkovalenko.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.request.project.*;
import ru.t1.zkovalenko.tm.dto.response.project.*;

public interface IProjectEndpoint {

    @NotNull ProjectChangeStatusByIdResponse projectChangeStatusById(@NotNull ProjectChangeStatusByIdRequest request);

    @NotNull ProjectChangeStatusByIndexResponse projectChangeStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request);

    @NotNull ProjectClearResponse projectClear(@NotNull ProjectClearRequest request);

    @NotNull ProjectCompleteByIdResponse projectCompleteById(@NotNull ProjectCompleteByIdRequest request);

    @NotNull ProjectCompleteByIndexResponse projectCompleteByIndex(@NotNull ProjectCompleteByIndexRequest request);

    @NotNull ProjectCreateResponse projectCreate(@NotNull ProjectCreateRequest request);

    @NotNull ProjectListResponse projectList(@NotNull ProjectListRequest request);

    @NotNull ProjectRemoveByIdResponse projectRemoveById(@NotNull ProjectRemoveByIdRequest request);

    @NotNull ProjectRemoveByIndexResponse projectRemoveByIndex(@NotNull ProjectRemoveByIndexRequest request);

    @NotNull ProjectShowByIdResponse projectShowById(@NotNull ProjectShowByIdRequest request);

    @NotNull ProjectShowByIndexResponse projectShowByIndex(@NotNull ProjectShowByIndexRequest request);

    @NotNull ProjectStartByIdResponse projectStartById(@NotNull ProjectStartByIdRequest request);

    @NotNull ProjectStartByIndexResponse projectStartByIndex(@NotNull ProjectStartByIndexRequest request);

    @NotNull ProjectUpdateByIdResponse projectUpdateById(@NotNull ProjectUpdateByIdRequest request);

    @NotNull ProjectUpdateByIndexResponse projectUpdateByIndex(@NotNull ProjectUpdateByIndexRequest request);

}
