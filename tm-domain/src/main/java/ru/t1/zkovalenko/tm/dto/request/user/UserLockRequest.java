package ru.t1.zkovalenko.tm.dto.request.user;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class UserLockRequest extends AbstractUserRequest {

    @Nullable
    private String login;

}
