package ru.t1.zkovalenko.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.model.Project;

public class ProjectStartByIdResponse extends AbstractProjectResponse {

    public ProjectStartByIdResponse(@Nullable Project project) {
        super(project);
    }

}
