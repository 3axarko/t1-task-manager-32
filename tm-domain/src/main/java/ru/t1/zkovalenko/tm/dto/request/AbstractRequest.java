package ru.t1.zkovalenko.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Getter
@Setter
public abstract class AbstractRequest implements Serializable {

    @Nullable
    private String userId;

}
