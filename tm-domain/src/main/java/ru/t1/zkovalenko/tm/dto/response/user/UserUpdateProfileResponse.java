package ru.t1.zkovalenko.tm.dto.response.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.model.User;

public class UserUpdateProfileResponse extends AbstractUserResponse {

    public UserUpdateProfileResponse(@Nullable final User user) {
        super(user);
    }

}
