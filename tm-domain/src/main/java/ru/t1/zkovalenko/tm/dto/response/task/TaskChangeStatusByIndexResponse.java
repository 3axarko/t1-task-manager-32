package ru.t1.zkovalenko.tm.dto.response.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.model.Task;

public class TaskChangeStatusByIndexResponse extends AbstractTaskResponse {

    public TaskChangeStatusByIndexResponse(@Nullable final Task task) {
        super(task);
    }

}
