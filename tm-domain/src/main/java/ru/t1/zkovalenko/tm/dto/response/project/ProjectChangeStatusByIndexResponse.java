package ru.t1.zkovalenko.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.model.Project;

public class ProjectChangeStatusByIndexResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIndexResponse(@Nullable final Project project) {
        super(project);
    }

}
