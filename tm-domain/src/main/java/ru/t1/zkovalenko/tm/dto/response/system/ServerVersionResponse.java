package ru.t1.zkovalenko.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import ru.t1.zkovalenko.tm.dto.response.AbstractResponse;

@Getter
@Setter
public class ServerVersionResponse extends AbstractResponse {

    private String version;

}
