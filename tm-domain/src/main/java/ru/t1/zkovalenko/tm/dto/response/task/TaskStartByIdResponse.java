package ru.t1.zkovalenko.tm.dto.response.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.model.Task;

public class TaskStartByIdResponse extends AbstractTaskResponse {

    public TaskStartByIdResponse(@Nullable Task task) {
        super(task);
    }

}
