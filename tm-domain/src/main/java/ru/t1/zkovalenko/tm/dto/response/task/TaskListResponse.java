package ru.t1.zkovalenko.tm.dto.response.task;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.response.AbstractResponse;
import ru.t1.zkovalenko.tm.model.Task;

import java.util.List;

@Getter
@Setter
public class TaskListResponse extends AbstractResponse {

    @NotNull
    private List<Task> tasks;

    public TaskListResponse(@NotNull final List<Task> tasks) {
        this.tasks = tasks;
    }

}
