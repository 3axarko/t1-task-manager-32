package ru.t1.zkovalenko.tm.dto.response.project;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.response.AbstractResponse;
import ru.t1.zkovalenko.tm.model.Project;

import java.util.List;

@Getter
@Setter
public class ProjectListResponse extends AbstractResponse {

    @NotNull
    private List<Project> projects;

    public ProjectListResponse(@NotNull final List<Project> projects) {
        this.projects = projects;
    }

}
