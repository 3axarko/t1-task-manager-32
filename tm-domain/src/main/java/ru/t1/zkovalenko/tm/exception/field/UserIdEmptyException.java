package ru.t1.zkovalenko.tm.exception.field;

public final class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("UserId is empty");
    }

}
