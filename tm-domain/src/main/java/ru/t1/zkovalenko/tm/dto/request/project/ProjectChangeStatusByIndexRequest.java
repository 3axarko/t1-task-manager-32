package ru.t1.zkovalenko.tm.dto.request.project;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.AbstractUserRequest;
import ru.t1.zkovalenko.tm.enumerated.Status;

@Getter
@Setter
public class ProjectChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private Status status;

}
