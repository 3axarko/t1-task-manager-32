package ru.t1.zkovalenko.tm.dto.request.task;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.AbstractUserRequest;

@Getter
@Setter
public class TaskRemoveByIdRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

}
