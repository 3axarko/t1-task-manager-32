package ru.t1.zkovalenko.tm.dto.response.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.model.Task;

public class TaskStartByIndexResponse extends AbstractTaskResponse {

    public TaskStartByIndexResponse(@Nullable Task task) {
        super(task);
    }

}
