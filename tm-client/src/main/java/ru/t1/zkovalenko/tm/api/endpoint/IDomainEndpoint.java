package ru.t1.zkovalenko.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.request.data.*;
import ru.t1.zkovalenko.tm.dto.response.data.*;

public interface IDomainEndpoint {

    @NotNull DataBackupLoadResponse dataBackupLoad(@NotNull DataBackupLoadRequest request);

    @NotNull DataBackupSaveResponse dataBackupSave(@NotNull DataBackupSaveRequest request);

    @NotNull DataBase64LoadResponse dataBase64Load(@NotNull DataBase64LoadRequest request);

    @NotNull DataBase64SaveResponse dataBase64Save(@NotNull DataBase64SaveRequest request);

    @NotNull DataBinaryLoadResponse dataBinaryLoad(@NotNull DataBinaryLoadRequest request);

    @NotNull DataBinarySaveResponse dataBinarySave(@NotNull DataBinarySaveRequest request);

    @NotNull DataJsonLoadJaxBResponse dataJsonLoadJaxB(@NotNull DataJsonLoadJaxBRequest request);

    @NotNull DataJsonLoadJFasterXmlResponse dataJsonLoadJFasterXml(@NotNull DataJsonLoadJFasterXmlRequest request);

    @NotNull DataJsonSaveFasterXmlResponse dataJsonSaveFasterXml(@NotNull DataJsonSaveFasterXmlRequest request);

    @NotNull DataJsonSaveJaxBResponse dataJsonSaveJaxB(@NotNull DataJsonSaveJaxBRequest request);

    @NotNull DataXmlLoadJaxBResponse dataXmlLoadJaxB(@NotNull DataXmlLoadJaxBRequest request);

    @NotNull DataXmlLoadJFasterXmlResponse dataXmlLoadJFasterXml(@NotNull DataXmlLoadJFasterXmlRequest request);

    @NotNull DataXmlSaveFasterXmlResponse dataXmlSaveFasterXml(@NotNull DataXmlSaveFasterXmlRequest request);

    @NotNull DataXmlSaveJaxBResponse dataXmlSaveJaxB(@NotNull DataXmlSaveJaxBRequest request);

    @NotNull DataYamlSaveFasterXmlResponse dataYamlSaveFasterXml(@NotNull DataYamlSaveFasterXmlRequest request);

    @NotNull DataYamlLoadFasterXmlResponse dataYamlLoadFasterXml(@NotNull DataYamlLoadFasterXmlRequest request);

}
