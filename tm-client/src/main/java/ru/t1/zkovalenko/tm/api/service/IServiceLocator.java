package ru.t1.zkovalenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    DomainEndpointClient getDomainEndpointClient();

    @NotNull
    ConnectionEndpointClient getConnectionEndpointClient();

    @NotNull
    SystemEndpointClient getSystemEndpointClient();

    @NotNull
    ProjectEndpointClient getProjectEndpointClient();

    @NotNull
    TaskEndpointClient getTaskEndpointClient();

    @NotNull
    UserEndpointClient getUserEndpointClient();

    @NotNull
    AuthEndpointClient getAuthEndpointClient();

}
