package ru.t1.zkovalenko.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.command.system.AbstractSystemCommand;

public class DisconnectCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "disconnect";

    @NotNull
    public static final String ARGUMENT = "-d";

    @NotNull
    public static final String DESCRIPTION = "Disconnect from server";

    @Override
    @SneakyThrows
    public void execute() {
        try {
            getServiceLocator().getConnectionEndpointClient().disconnect();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
