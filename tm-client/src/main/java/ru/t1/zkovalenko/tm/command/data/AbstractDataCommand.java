package ru.t1.zkovalenko.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.client.DomainEndpointClient;
import ru.t1.zkovalenko.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected DomainEndpointClient getDomainEndpointClient() {
        return serviceLocator.getDomainEndpointClient();
    }

}
