package ru.t1.zkovalenko.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.zkovalenko.tm.api.repository.ICommandRepository;
import ru.t1.zkovalenko.tm.api.service.ICommandService;
import ru.t1.zkovalenko.tm.api.service.ILoggerService;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.api.service.IServiceLocator;
import ru.t1.zkovalenko.tm.client.*;
import ru.t1.zkovalenko.tm.command.AbstractCommand;
import ru.t1.zkovalenko.tm.command.server.ConnectCommand;
import ru.t1.zkovalenko.tm.command.server.DisconnectCommand;
import ru.t1.zkovalenko.tm.exception.system.CommandNotSupportedException;
import ru.t1.zkovalenko.tm.repository.CommandRepository;
import ru.t1.zkovalenko.tm.service.CommandService;
import ru.t1.zkovalenko.tm.service.LoggerService;
import ru.t1.zkovalenko.tm.service.PropertyService;
import ru.t1.zkovalenko.tm.util.ColorizeConsoleTextUtil;
import ru.t1.zkovalenko.tm.util.SystemUtil;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.zkovalenko.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ConnectionEndpointClient connectionEndpointClient = new ConnectionEndpointClient();

    @NotNull
    private final AuthEndpointClient authEndpointClient = new AuthEndpointClient();

    @NotNull
    private final SystemEndpointClient systemEndpointClient = new SystemEndpointClient();

    @NotNull
    private final DomainEndpointClient domainEndpointClient = new DomainEndpointClient();

    @NotNull
    private final ProjectEndpointClient projectEndpointClient = new ProjectEndpointClient();

    @NotNull
    private final TaskEndpointClient taskEndpointClient = new TaskEndpointClient();

    @NotNull
    private final UserEndpointClient userEndpointClient = new UserEndpointClient();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void connect() {
        processCommand(ConnectCommand.NAME);
    }

    private void disconnect() {
        processCommand(DisconnectCommand.NAME);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void processArgument(@Nullable final String argument) {
        if (argument == null) throw new CommandNotSupportedException();
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new CommandNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void exit() {
        System.exit(0);
    }

    private void prepareShutdown() {
        loggerService.info("** Task-Manager is shutting down **");
        fileScanner.stop();
        disconnect();
    }

    public void run(@Nullable String[] args) {
        if (processArguments(args)) exit();

        prepareStartup();
//        afterStartCommands();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("Enter Command:");
                @NotNull final String command = TerminalUtil.nextLine();
                System.out.println("---");
                processCommand(command);
                ColorizeConsoleTextUtil.greenText("[OK]");
                loggerService.command(command);
            } catch (@Nullable final Exception e) {
                ColorizeConsoleTextUtil.redText("[FAIL]");
                loggerService.error(e);
            }
        }
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** Welcome to Task-Manager **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
        connect();
    }

    public void processCommand(@Nullable final String command) {
        if (command == null) throw new CommandNotSupportedException();
        @Nullable AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) abstractCommand = commandService.getCommandByArgument(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

}
